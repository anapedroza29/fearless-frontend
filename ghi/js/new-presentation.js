window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/conferences/"
    const response = await fetch(url)
    if(response.ok){
        const data = await response.json();
        const selectTag = document.getElementById("conference");
        for (let conference of data.conferences){
            let option = document.createElement("option")
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option)
            
     }
    }
    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const forData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(forData));
        const consferenceSelect = document.getElementById("conference");
        console.log(consferenceSelect)
        const presentationUrl =	`http://localhost:8000/api/conferences/${consferenceSelect.value}/presentations/`
        console.log("presentationUrl", presentationUrl)
        const fetchConfig = {
            method: "POST",
            body: json,
            header: {
                'Content-Type': 'application/json',
            }
            
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if(response.ok){
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation)
        }
    })
});