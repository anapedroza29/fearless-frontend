function createCard(name, description, pictureUrl, start, end, city) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${city}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
            <div>Start date: <span>${start}</span></div>
            <div>End date: <span>${end}</span></div>
          </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    
    try {
        const response = await fetch(url);
    
        if (!response.ok) {
        // Figure out what to do when the response is bad
        const alert = document.createElement('div');
        alert.classList.add('alert', 'alert-danger');
        alert.innerHTML = 'Error fetching conferences data, please try again later.';
        const container = document.querySelector('.container');
        container.parentNode.insertBefore(alert, container)
        
        } else {
        const data = await response.json();
    
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            console.log("detailResponse", detailResponse)
            if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toLocaleDateString()
            const end = new Date (details.conference.ends).toLocaleDateString()
            const city = details.conference.location.name
            const html = createCard(title, description, pictureUrl, start, end, city );
            const column = document.querySelector('.col');
            column.innerHTML += html;
            console.log(html);
            }
        }
    
        }
    } catch (e) {
        const alert = document.createElement('div');
        alert.classList.add('alert', 'alert-danger');
        alert.innerHTML = 'Error fetching conferences data, please try again later.';
        const container = document.querySelector('.container');
        container.parentNode.insertBefore(alert, container);
        console.error(e)
    }
    
    });

  

  
