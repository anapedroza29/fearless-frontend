import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
    return (
        <React.Fragment>
           
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference GO!</a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/mainpage">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/locations/new">New location</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/conferences/new">New conference</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/attendees/new">New attendee</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/attendees">Attendee</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/presentations/new">New presentation</NavLink>
                    </li>
                </ul>
                </div>
            </div>
            </nav>
            
            
        </React.Fragment>
      );
    }
export default Nav;